//
//  CustomUIView.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/25/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUIView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    

}
