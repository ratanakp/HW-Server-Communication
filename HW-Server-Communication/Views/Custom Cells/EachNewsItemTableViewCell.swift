//
//  EachNewsItemTableViewCell.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/25/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class EachNewsItemTableViewCell: UITableViewCell {

    @IBOutlet weak var eachItemImage: CustomUIImageView!
    @IBOutlet weak var eachItemLabel: UILabel!
    @IBOutlet weak var eachItemDate: UILabel!
    @IBOutlet weak var eachItemSaveButton: CustomUIButton!
    
    var isSaveImageError: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func eachItemSaveButtonAction(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(eachItemImage.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error == nil {
            isSaveImageError = true
            let ac = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Found some error", style: .default, handler: nil))
            
           
            
            // let vc = ViewController()
            // vc.present(ac, animated: true, completion: nil)
            
           
        } else {
            isSaveImageError = false
            let ac = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Found some error", style: .default, handler: nil))
        }
    }
}
