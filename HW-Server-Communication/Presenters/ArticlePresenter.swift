//
//  ArticlePresenter.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import Foundation

class ArticlePresenter {
    
    var delegate: ArticlePresenterProtocol?
    var articleService: ArticleService?
    
    init() {
        self.articleService = ArticleService()
        articleService?.delegate = self
    }
    
    
    func getArticle(page: Int, limit: Int) {
        articleService?.fetchArticle(page: page, limit: limit)
    }
    
    func saveArticle(article: Article, image: Data) {
        self.articleService?.saveArticle(article: article, image: image)
    }
    
    func deleteArticle(articleId: Int) {
        self.articleService?.deleteArticle(articleId: articleId)
    }
    
}

extension ArticlePresenter: ArticleServiceProtocol {
    func didResponseData(articles: [Article]) {
        self.delegate?.didResponseData(articles: articles)
    }
    func didResponseStatus(message: String, status: Bool) {
        self.delegate?.didResponseStatus(message: message, status: status)
    }
}

/*extension ArticlePresenter: ArticleServiceProtocol {
    public func didResponseData(articles: [Article]) {
        self.delegate?.didResponseData(articles: articles)
    }
}*/
