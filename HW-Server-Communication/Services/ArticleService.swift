//
//  ArticleService.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ArticleService {
    var delegate: ArticleServiceProtocol?
    
    var articles: [Article] = [Article]()
    var countArticle: Int?
    
    var url = "http://api-ams.me/v1/api/articles"
    var headers = ["Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=", "Accept": "application/json"]
    
    func fetchArticle(page: Int, limit: Int) {
        let get_url = "\(url)?page=\(page)&limit=\(limit)"
        
        Alamofire.request(get_url, method: HTTPMethod.get, parameters: nil, headers: headers).responseJSON{
            (response) in
        
            if let value = response.result.value {
                let json = JSON(value)
                //print(json["DATA"])
//                let id: Int = json["DATA"][0]["ID"].int!
//                print("This is my ID: \(String(describing: id))")
                
                if let getArticles = Array<Article>(JSONString: "\(json["DATA"])"){
                    self.articles = getArticles
                    self.countArticle = self.articles.count
                    
                    //print("Some articles found: \(String(describing: self.countArticle))")
                    //print(self.articles)
                    
                    // Back to json
                    /*let toJSON = self.articles[0].toJSONString()
                    print(toJSON!)*/
                    
                     //let toJSON1 = Mapper().toJSONString(self.articles[0], prettyPrint: true)
                    // print(toJSON1!)
                    
                    self.delegate?.didResponseData(articles: self.articles)
                    print("Fetch data success...")
                    
                }
            }
            else if (response.result.error == nil) {
                print("Found some error!!!")
            }
            else {
                print("Not error and not success...")
            }
        }
    }
    
    func deleteArticle(articleId: Int) {
        Alamofire.request("\(url)/\(articleId)", method: .delete, headers: headers)
            .responseJSON { response in
                if let error = response.result.error {
                    print("Error: \(error.localizedDescription)")
                    self.delegate?.didResponseStatus(message: "Error: \(error.localizedDescription)", status: false)
                    
                } else {
                    print("==============> :\(articleId) is deleted.")
                    self.delegate?.didResponseStatus(message: "Article deleted!!!", status: true)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
        }
    }
    
    
    
    func saveArticle(article: Article, image: Data) {
        
        let upload_img_url = "http://api-ams.me/v1/api/uploadfile/single"
        var article: Article = article
        
        Alamofire.upload(multipartFormData: { (multipart) in
            
            multipart.append(image, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
            
        }, to: upload_img_url, method: .post, headers: headers) { (encoding) in
            
            switch encoding {
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                
                upload.responseJSON(completionHandler: { (response) in
                    
                    if let data = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any] {
                        
                        let image_url = data["DATA"] as! String
                        article.image = image_url
                        
                        var parameters: Parameters = [
                            "TITLE" : article.title!,
                            "DESCRIPTION" : article.desc!
                        ]
                        
                        if image != UIImageJPEGRepresentation(#imageLiteral(resourceName: "ic_menu"), 1) {
                            
                            parameters = [
                                "TITLE" : article.title!,
                                "DESCRIPTION" : article.desc!,
                                "IMAGE" : article.image!
                            ]
                            
                        }
                        
                        if article.id == 0 {
                            Alamofire.request("\(self.url)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
                                if response.result.isSuccess {
                                    self.delegate?.didResponseStatus(message: "Saved successfully!!!", status: true)
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                }
                            }
                        }else {
                            Alamofire.request("\(self.url)/\(article.id!)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
                                if response.result.isSuccess {
                                    self.delegate?.didResponseStatus(message: "Updated successfully!!!", status: true)
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                }
                            }
                        }
                        
                    }
                    
                })
                
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            }
            
        }
    }
}









