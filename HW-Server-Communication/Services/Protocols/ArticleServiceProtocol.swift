//
//  ArticleServiceProtocol.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import Foundation

protocol ArticleServiceProtocol {
    func didResponseData(articles: [Article])
    func didResponseStatus(message: String, status: Bool)
}
