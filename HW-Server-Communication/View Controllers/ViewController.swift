//
//  ViewController.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/22/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit
import Kingfisher
import Photos


class ViewController: UIViewController, ArticlePresenterProtocol{
   
    var refreshControl: UIRefreshControl!
    var page = 1
    var url: URL?
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    @objc func refreshTable() {
        self.articles.removeAll()
        page = 1
        self.fetchReloadData(page: page)
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.articles.removeAll()
        fetchReloadData(page: 1)
        self.navigationController?.navigationBar.topItem?.title = "Home"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkPhotoLibraryPermission()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Data...", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
        refreshControl.tintColor = UIColor.red
        refreshControl.backgroundColor = UIColor.lightGray
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        self.homeTableView.addSubview(refreshControl)
        
        //Article Delegate*****************************************************************************************************************************
        articlePresenter = ArticlePresenter()
        self.articlePresenter?.delegate = self
        
        //Tableview delegate*****************************************************************************************************************************
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        self.homeTableView.backgroundColor = UIColor.lightGray//UIColor(displayP3Red: 0, green: 128/255, blue: 128/255, alpha: 0.5)
        
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    //custom table cell function
    func customCell(cell: UITableViewCell) {
        //cell.contentView.backgroundColor = UIColor.clear
        let whiteRoundedeView: UIView = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width - 16, height: 115))
        whiteRoundedeView.layer.backgroundColor = UIColor.white.cgColor
        whiteRoundedeView.layer.cornerRadius = 5.0
        whiteRoundedeView.layer.masksToBounds = false
        whiteRoundedeView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedeView.layer.shadowOpacity = 0.5
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        //cell.contentView.addSubview(whiteRoundedeView)
        //cell.contentView.sendSubview(toBack: whiteRoundedeView)
    }
    
    //*****************************************************************************************************************************
    
    var articles = [Article]()
    var articlePresenter: ArticlePresenter?
    
    func didResponseData(articles: [Article]) {
        self.articles += articles
        self.homeTableView.reloadData()
        print("Punak")
        
    }
    func didResponseStatus(message: String, status: Bool) {
        showMessage(title: message, message: nil)
    }
    
    func fetchReloadData(page: Int){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        articlePresenter?.getArticle(page: page, limit: 15)
        homeTableView.reloadData()
    }
    
    func checkPhotoLibraryPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    // DO WHAT YOU WANT
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }
    
}




extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let height = scrollView.frame.size.height
        let contentYOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYOffset
        
        if distanceFromBottom < height {
            page = page + 1
            self.homeTableView.tableFooterView = indicator
            self.homeTableView.tableFooterView?.isHidden = false
            self.homeTableView.tableFooterView?.center = indicator.center
            self.indicator.startAnimating()
            fetchReloadData(page: page)
            self.indicator.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("sjfklsdjfdklsjfkl: \(self.articles.count)")
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        url = nil
        if indexPath.row == 0 {
            if let myURL = articles[articles.count - 1].image {
                url = URL(string: myURL)
            }
            
            let cell = Bundle.main.loadNibNamed("SliderTableViewCell", owner: self, options: nil)?.first as! SliderTableViewCell
            
            cell.sliderImageView.kf.setImage(with: url)
            cell.sliderTitleLabel.text = articles[articles.count - 1].title
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.view.frame.size.width)
            customCell(cell: cell)
            
            return cell
        }
        else {
            if let myURL = articles[indexPath.row - 1].image {
                url = URL(string: myURL)
            }
            
            let cell = Bundle.main.loadNibNamed("EachNewsItemTableViewCell", owner: self, options: nil)?.first as! EachNewsItemTableViewCell
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.view.frame.size.width)
            cell.layer.backgroundColor = UIColor.red.cgColor
            customCell(cell: cell)
            
            cell.eachItemLabel.text = articles[indexPath.row - 1].title
            cell.eachItemImage.kf.setImage(with: url)
            
            return cell
        }
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        if indexPath.row != 0 {
    //            return 125
    //        }
    //        else {
    //            return tableView.rowHeight
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        customCell(cell: cell)
        
        /*let clearView = UIView()
         clearView.backgroundColor = UIColor.clear // Whatever color you like
         UITableViewCell.appearance().selectedBackgroundView = clearView*/
    }
    
    
    /*func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            print("PuNak you")
            
            
        }
    }*/
    
    /*func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 5
     }
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return 5
     }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let detailStoryBoard = storyBoard.instantiateViewController(withIdentifier: "DetailViewBundleID") as! DetailViewController
            
            detailStoryBoard.article = articles[indexPath.row - 1]
            
            self.navigationController?.pushViewController(detailStoryBoard, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.row != 0 {
            
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, index) in
                
                if let addArticleViewControllerStoryboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddArticleStoryboardID") as? AddArticleViewController {
                    
                    addArticleViewControllerStoryboard.article = self.articles[indexPath.row - 1]
                    print("article: \(self.articles[indexPath.row - 1])")
                    addArticleViewControllerStoryboard.isUpdate = true
                    
                    if let navigator = self.navigationController {
                        navigator.pushViewController(addArticleViewControllerStoryboard, animated: true)
                        //navigator.navigationBar.topItem?.title = "Update"
                    }
                }
                
            }
            
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, index) in
                
                print("deleted \(indexPath.item - 1)")
                self.showConfirmDelete(title: "Are you sure to delete?", message: "", index: indexPath.item - 1)
                
            }
            
            return [delete, edit]
        }
        
        return []
    }
    
    func showConfirmDelete(title: String, message: String, index: Int) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
            self.articlePresenter?.deleteArticle(articleId: self.articles[index].id!)
            self.articles.remove(at: index)
            self.homeTableView.reloadData()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func showMessage(title: String, message: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
}















