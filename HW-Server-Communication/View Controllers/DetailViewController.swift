//
//  DetailViewController.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit
import Kingfisher
import ObjectMapper
import Toast_Swift


class DetailViewController: UIViewController {
    
    var article: Article?

    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTitleTextView: UITextView!
    @IBOutlet weak var detailDescriptionTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveImageBarButtton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_save"), style: .plain, target: self, action: #selector(saveImageHandler))
        navigationItem.rightBarButtonItem = saveImageBarButtton
        
    }
    
    @objc func saveImageHandler() {
        UIImageWriteToSavedPhotosAlbum(detailImageView.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        print("save image handle")
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            //present(ac, animated: true)
            // toast with a specific duration and position
            self.view.makeToast("Image saved!!!", point: CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height - 70), title: nil, image: image, completion: nil)
            //self.view.makeToast("Image saved!!!", duration: 3.0, position: .bottom)
        }
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let url: URL?
        if let myURL = article?.image {
            url = URL(string: myURL)
            
            self.detailImageView.kf.setImage(with: url)
        }
        self.detailTitleTextView.text = article?.title
        self.detailDescriptionTextView.text = article?.desc
        
        let toJSON = Mapper().toJSONString(article!, prettyPrint: true)
        print("========> Detail Page (Article): \(String(describing: toJSON))")
    }
}
