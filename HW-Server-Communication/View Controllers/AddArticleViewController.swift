//
//  AddArticleViewController.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/27/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

class AddArticleViewController: UIViewController, UIGestureRecognizerDelegate, ArticlePresenterProtocol{
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var saveArticleBarButton: UIBarButtonItem!
    
    func didResponseData(articles: [Article]) {
        
    }
    
    func didResponseStatus(message: String, status: Bool) {
        print("Status \(status)")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var addArticleImageView: CustomUIImageView!
    @IBOutlet weak var addArticleTitleTextField: UITextView!
    @IBOutlet weak var addArticleDescTextField: UITextView!
    @IBOutlet weak var chooseImageLabel: UILabel!
    @IBOutlet weak var chooseImageUIView: UIView!
    @IBOutlet weak var chooseImageSubUIView: CustomUIView!
    
    let imagePicker = UIImagePickerController()
    var articlePresenter = ArticlePresenter()
    
    var isUpdate = false
    var article: Article = Article()
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        
        //Long press recognizr*****************************************************************************************************************************
        
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5 // 1 second press
        longPressGesture.allowableMovement = 15 // 15 points
        longPressGesture.delaysTouchesBegan = false
        longPressGesture.delegate = self
        
        self.addArticleImageView.isUserInteractionEnabled = true
        self.chooseImageLabel.isUserInteractionEnabled = true
        self.chooseImageUIView.isUserInteractionEnabled = true
        self.chooseImageSubUIView.isUserInteractionEnabled = true
        
        self.addArticleImageView.addGestureRecognizer(longPressGesture)
        self.chooseImageLabel.addGestureRecognizer(longPressGesture)
        self.chooseImageUIView.addGestureRecognizer(longPressGesture)
        self.chooseImageSubUIView.addGestureRecognizer(longPressGesture)
        // longPressGesture.delegate = self
        // End long press recegnizer
        
        imagePicker.delegate = self
        
        
        
        
        //TextView****************************************************************************************************************************
        if isUpdate == false {
            self.addArticleTitleTextField.text = "Title"
            self.addArticleDescTextField.text = "Description"
            self.addArticleTitleTextField.textColor = UIColor.lightGray
            self.addArticleDescTextField.textColor = UIColor.lightGray
            
            print("This is isNew")
        }
        
        
        self.addArticleDescTextField.delegate = self
        self.addArticleTitleTextField.delegate = self
        
        
        
        //User Input textview
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        
        // User update article***********************************************************************************************************
        if isUpdate {
            let url: URL?
            if let myURL = article.image {
                url = URL(string: myURL)
                
                self.addArticleImageView.kf.setImage(with: url)
            }
            
            self.addArticleTitleTextField.text = article.title
            self.addArticleDescTextField.text = article.desc
            
            print("This is isUpdate")
            print("Article: \(article)")
            //isUpdate = false
            //self.navigationController?.navigationBar.topItem?.title = "Update"
            self.navigationBar.title = "Update Article"
            self.saveArticleBarButton.title = "Update"
            
            
        }
        
        
        
    }
    
    
    
    // Handle User Input textview
    @objc func adjustForKeyboard(notification: Notification) {
        
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            addArticleDescTextField.contentInset = UIEdgeInsets.zero
        } else {
            addArticleDescTextField.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 200, right: 0)
        }
        
    }
    // End Handle User Input textview

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.leftItemsSupplementBackButton = false
        
        if isUpdate {
            let url: URL?
            if let myURL = article.image {
                url = URL(string: myURL)
                
                self.addArticleImageView.kf.setImage(with: url)
            }
            self.addArticleTitleTextField.text = article.title
            self.addArticleDescTextField.text = article.desc
            
            //isUpdate = false
            print("This is article: \(article)")
            self.saveArticleBarButton.title = "Update"
            
        }
    }
    
    // Receive action
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        
        if gestureRecognizer.state == UIGestureRecognizerState.began {
            
            let alert = UIAlertController(title: "OK you", message: "OK you you", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: nil))
            //self.present(alert, animated: true, completion: nil)
            
            //Camera
            /*AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
             if response {
             //access granted
             } else {
             
             }
             }*/
            openPhoto()
        }
        
        
        
    }
    func openPhoto() {
        imagePicker.delegate = self
        
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            print("can't open photo library")
            return
        }
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // Save button action
    @IBAction func saveArticle(_ sender: Any) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let image = UIImageJPEGRepresentation(self.addArticleImageView.image!, 1)
        if isUpdate {
            article.title = addArticleTitleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            article.desc = addArticleDescTextField.text.trimmingCharacters(in: .whitespacesAndNewlines)
            articlePresenter.saveArticle(article: article, image: image!)
        }
        else {
            article.id = 0
            article.title = addArticleTitleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            article.desc = addArticleDescTextField.text.trimmingCharacters(in: .whitespacesAndNewlines)
            articlePresenter.saveArticle(article: article, image: image!)
        }
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    
}


extension AddArticleViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            addArticleImageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        defer {
            picker.dismiss(animated: true)
        }
        
        print("did cancel")
    }
}


extension AddArticleViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.textColor == UIColor.lightGray{
            textView.textColor = UIColor.blue
            textView.text = ""
            textView.text = nil
            textView.textColor = UIColor.black
        }
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.text == nil || textView.text == "" {
            textView.textColor = UIColor.lightGray
            if textView == self.addArticleTitleTextField {
                textView.text = "Title"
            }
            else if textView == self.addArticleDescTextField {
                textView.text = "Description"
            }
        }
        return true
    }
}






































