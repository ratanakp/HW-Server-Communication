//
//  Pagination.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct Pagination: Mappable {
    var page: Int?
    var limit: Int?
    var total_count: String?
    var total_pages: String?
    
    public init?(map: Map) {
        if map.JSON["PAGE"] == nil {
            return  nil
        }
    }
    
    public mutating func mapping(map: Map) {
        page <- map["PAGE"]
        limit <- map["LIMIT"]
        total_count <- map["TOTAL_COUNT"]
        total_pages <- map["TOTAL_PAGES"]
    }
    
    
    
}
