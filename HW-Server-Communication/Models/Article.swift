//
//  Article.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import Foundation
import ObjectMapper

struct Article {

    var id: Int?
    var title: String?
    var desc: String?
    var image: String?
    var date: Date?
    
    var author: Author?
    var category: Category?
    var pagination: Pagination?
    
    
}
extension Article: Mappable{
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["ID"]
        title <- map["TITLE"]
        desc <- map["DESCRIPTION"]
        image <- map["IMAGE"]
        date <- map["CREATED_DATE"]
        
        author <- map["AUTHOR"]
        category <- map["CATEGORY"]
        pagination <- map["PAGINATION"]
    }
}

