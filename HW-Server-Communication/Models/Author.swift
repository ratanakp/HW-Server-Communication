//
//  Author.swift
//  HW-Server-Communication
//
//  Created by Ratanak Phang on 12/26/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct Author: Mappable{
    var id: Int?
    var name: String?
    var gender: String?
    var telephone: String?
    
    public init?(map: Map) {
        if map.JSON["NAME"] == nil {
            return nil
        }
    }
    
    public mutating func mapping(map: Map) {
        id <- map["ID"]
        name <- map["NAME"]
        gender <- map["GENDER"]
        telephone <- map["TELEPHONE"]
    }
    
    
}
